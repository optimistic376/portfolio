// // Import jQuery module (npm i jquery)
import $ from 'jquery'
window.jQuery = $
window.$ = $

// // Import vendor jQuery plugin example (not module)
// require('~/app/libs/mmenu/dist/mmenu.js')
require('./libs/bs/bootstrap.min');
require('./libs/bs/popper.min');
require('./libs/sliding-menu/jquery-simple-mobilemenu');
require('./libs/star/dobtcostar');
require('./libs/validation/additional-methods.min');
require('./libs/validation/jquery.validate.min')


$(function() {
	$('.star').starrr({
		rating: 4
	})
	$(".mobile_menu").simpleMobileMenu({
			onMenuLoad: function(menu) {
			},
			onMenuToggle: function(menu, opened) {
			},
			"menuStyle": "slide"
	});
	$(".js-search-toggle").on("click", () => {
		if($(window).width() < 1366) {
			$(".header").toggleClass("search-active");
		}
		$(".search-bar").toggleClass("open");
	})
	$(document).on('click', ".js-dropdown-toggle", ()=> {
		$('.drop-menu').toggleClass("open");
	})

	$(window).on( 'scroll', () => {
		if ($(this).scrollTop() > 0) {
			$('.js-scroll-top').fadeIn();
		} else {
			$('.js-scroll-top').fadeOut();
		}
	});
	$(document).on("click", ".js-scroll-top", () => {
		$('body, html').animate({
			scrollTop: 0
		}, 400);
		return false;
	})




	// input animation
	$('input, textarea').each(function() {
		if (this.value) {
			$(this).parent().find('label').addClass('active');
		}
	});

	$('input, textarea').on('focusin', function() {
		$(this).parent().find('label').addClass('active');
	});
	 
	$('input, textarea').on('focusout', function() {
		if (!this.value) {
			$(this).parent(".form-group").find('label').removeClass('active');
		}
	});

	$(document).on("click", ".card-header", function() {
		if($(this).hasClass('opened')) {
			$(this).removeClass('opened');
		} else {
			$(".accordion").find(".card-header").removeClass("opened");
			$(this).addClass('opened');
		}
	})

});
$('.button').on('click', function(e){
	e.preventDefault();
})



const btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});



$('.menu-btn').on('click', function(e){
	e.preventDefault();
	$('.menu').toggleClass('active');
	$('.menu-overlay').toggleClass('active');
})

$('.close').on('click', function(e){
	e.preventDefault();
	if($('.menu').hasClass('active')){
		$('.menu').removeClass('active');
		$('.menu-overlay').removeClass('active');
	}
})


$('.menu-list__item').on('click', function(e){
	e.preventDefault();
	$('.aside-menu').toggleClass('active');
})

$('.close').on('click', function(e){
	e.preventDefault();
	if($('.aside-menu').hasClass('active')){
		$('.aside-menu').removeClass('active');
	}
})
$('.aside-menu__top-link').on('click', function(e){
	e.preventDefault();
	if($('.aside-menu').hasClass('active')){
		$('.aside-menu').removeClass('active');
	}
})



$('.search-btn').on('click', function(e){
	e.preventDefault();
	$('.search').toggleClass('active');
	$('.search__main').toggleClass('active');
	$('body').toggleClass('hidden');
})

$('.search-close').on('click', function(e){
	e.preventDefault();
	if($('.search').hasClass('active')){
		$('.search').removeClass('active');
		$('body').removeClass('hidden');
		$('.search__main').toggleClass('active');
	}
})


//for dosage select

document.addEventListener('DOMContentLoaded', () => {
	/////////////////////
	// SELECT LIB  
	////////////////////
  
	var x, i, j, l, ll, selElmnt, a, b, c;
	/* Look for any elements with the class "custom-select": */
	x = document.getElementsByClassName("custom-select");
	l = x.length;
	for (i = 0; i < l; i++) {
	  selElmnt = x[i].getElementsByTagName("select")[0];
	  ll = selElmnt.length;
	  /* For each element, create a new DIV that will act as the selected item: */
	  a = document.createElement("DIV");
	  a.setAttribute("class", "select-selected");
	  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	  x[i].appendChild(a);
	  /* For each element, create a new DIV that will contain the option list: */
	  b = document.createElement("DIV");
	  b.setAttribute("class", "select-items select-hide");
	  for (j = 1; j < ll; j++) {
		/* For each option in the original select element,
		create a new DIV that will act as an option item: */
		c = document.createElement("DIV");
		c.innerHTML = selElmnt.options[j].innerHTML;
		c.addEventListener("click", function(e) {
			/* When an item is clicked, update the original select box,
			and the selected item: */
			var y, i, k, s, h, sl, yl;
			s = this.parentNode.parentNode.getElementsByTagName("select")[0];
			sl = s.length;
			h = this.parentNode.previousSibling;
			for (i = 0; i < sl; i++) {
			  if (s.options[i].innerHTML == this.innerHTML) {
				s.selectedIndex = i;
				h.innerHTML = this.innerHTML;
				y = this.parentNode.getElementsByClassName("same-as-selected");
				yl = y.length;
				for (k = 0; k < yl; k++) {
				  y[k].removeAttribute("class");
				}
				this.setAttribute("class", "same-as-selected");
				break;
			  }
			}
			h.click();
		});
		b.appendChild(c);
	  }
	  x[i].appendChild(b);
	  a.addEventListener("click", function(e) {
		/* When the select box is clicked, close any other select boxes,
		and open/close the current select box: */
		e.stopPropagation();
		closeAllSelect(this);
		this.nextSibling.classList.toggle("select-hide");
		this.classList.toggle("select-arrow-active");
	  });
	}
  
	function closeAllSelect(elmnt) {
	  /* A function that will close all select boxes in the document,
	  except the current select box: */
	  var x, y, i, xl, yl, arrNo = [];
	  x = document.getElementsByClassName("select-items");
	  y = document.getElementsByClassName("select-selected");
	  xl = x.length;
	  yl = y.length;
	  for (i = 0; i < yl; i++) {
		if (elmnt == y[i]) {
		  arrNo.push(i)
		} else {
		  y[i].classList.remove("select-arrow-active");
		}
	  }
	  for (i = 0; i < xl; i++) {
		if (arrNo.indexOf(i)) {
		  x[i].classList.add("select-hide");
		}
	  }
	}
  
	/* If the user clicks anywhere outside the select box,
	then close all select boxes: */
	document.addEventListener("click", closeAllSelect);
  
  })



//for order-select

  var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("order-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByClassName("options")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "options-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-item select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByClassName("options")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-item");
  y = document.getElementsByClassName("options-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);



$('.faq_item_title').on('click',function(){
        $(this).parents('.faq_item').find('.faq_item_body').slideToggle(300);
        $(this).toggleClass('open');
        if ($(this).hasClass('show_all')){
                if ($(this).hasClass('open')) {
                    $(this).html('Свернуть все');
                $('.faq_item_title_inner:not(.open)').trigger('click');
            } else {
                $(this).html('Смотреть все');
                $('.faq_item_title_inner.open').trigger('click');
            }
        }
    });






	const selectSingle = document.querySelector('.__select');
	const selectSingle_title = selectSingle.querySelector('.__select__title');
	const selectSingle_labels = selectSingle.querySelectorAll('.__select__label');

	const selectYear = document.querySelector('.__select__year');
	const selectYear_title = document.querySelector('.__select__year__title');
	const selectYear_labels = selectYear.querySelectorAll('.__select__year__label');
	
	// Toggle menu
	selectSingle_title.addEventListener('click', () => {
	  if ('active' === selectSingle.getAttribute('data-state')) {
		selectSingle.setAttribute('data-state', '');
	  } else {
		selectSingle.setAttribute('data-state', 'active');
	  }
	});

	selectYear_title.addEventListener('click', () => {
		if ('active' === selectYear.getAttribute('data-state')) {
			selectYear.setAttribute('data-state', '');
		} else {
			selectYear.setAttribute('data-state', 'active');
		}
	  });
	
	// Close when click to option
	for (let i = 0; i < selectSingle_labels.length; i++) {
	  selectSingle_labels[i].addEventListener('click', (evt) => {
		selectSingle_title.textContent = evt.target.textContent;
		selectSingle.setAttribute('data-state', '');
	  });
	}

	for (let i = 0; i < selectYear_labels.length; i++) {
		selectYear_labels[i].addEventListener('click', (evt) => {
			selectYear_title.textContent = evt.target.textContent;
			selectYear.setAttribute('data-state', '');
		});
	}
	









  